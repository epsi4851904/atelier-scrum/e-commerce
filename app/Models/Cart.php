<?php

namespace App\Models;

class Cart
{
    function addToCart($productId) {
        $cart = isset($_COOKIE['cart']) ? json_decode($_COOKIE['cart'], true) : [];
        $cart[] = $productId;
        setcookie('cart', json_encode($cart), time() + (86400 * 30), '/');
    }
    
    function getCart() {
        return isset($_COOKIE['cart']) ? json_decode($_COOKIE['cart'], true) : [];
    }
    
    function removeFromCart($productId) {
        $cart = isset($_COOKIE['cart']) ? json_decode($_COOKIE['cart'], true) : [];
        $index = array_search($productId, $cart);
    
        if ($index !== false) {
            unset($cart[$index]);
        }

        setcookie('cart', json_encode(array_values($cart)), time() + (86400 * 30), '/');
    }
}
