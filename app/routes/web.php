<?php

use App\Route;

Route::get(['/', 'App\Controllers\HomeController@index'])->name('app_index');
Route::get(['/api/test', 'App\Controllers\HomeController@apiExemple'])->name('api_exemple');

Route::get(['/panier', 'App\Controllers\ShopController@index'])->name('shop');
Route::get(['/panier/{id}', 'App\Controllers\ShopController@create'])->name('shop_create');
Route::get(['/panier/{id}/delete', 'App\Controllers\ShopController@delete'])->name('shop_delete');