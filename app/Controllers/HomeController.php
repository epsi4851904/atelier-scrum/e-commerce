<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Cart;

class HomeController extends Controller
{
    public function index()
    {
        $data = [
            "products" => [
                [
                    "id" => 1,
                    "name" => "T-shirt",
                    "description" => "Comfortable cotton t-shirt available in various colors.",
                    "price" => 19.99,
                    "image" => "https://beauf-land.com/cdn/shop/products/t-shirt-ricard-humour-2.jpg?v=1612345791"
                ],
                [
                    "id" => 2,
                    "name" => "short",
                    "description" => "Classic denim jeans with a modern fit.",
                    "price" => 39.99,
                    "image" => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4g8oO2Hv9xWf9IOR7pzYcNy7X8257nKz7UQ&usqp=CAU",
                    "category" => "Clothing"
                ],
                [
                    "id" => 3,
                    "name" => "bob",
                    "description" => "Lightweight running shoes for enhanced performance.",
                    "price" => 19.99,
                    "image" => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3508kLjf4STlG_m8KIugJ5BMz8rrbqKx8uw&usqp=CAU",
                    "category" => "Footwear"
                ],
                [
                    "id" => 4,
                    "name" => "Ceinture bière",
                    "description" => "High-performance smartphone with advanced features.",
                    "price" => 499.99,
                    "image" => "https://bleublancbeauf.com/cdn/shop/products/ceinture-biere-details-beauf_2048x2048.jpg?v=1613367441",
                    "category" => "Electronics"
                ]
            ]
        ];
        $cart = new Cart();
        $carts = $cart->getCart();
        $nbCarts = count($carts);
        return $this->twig->render('home/index.html.twig', compact('data', 'nbCarts'));
    }

    public function apiExemple()
    {
        // Exemple de route API REST
        $data = [
            'message' => 'Ceci est un retour en json pour réaliser une api'
        ];
        
        return jsonResponse(self::HTTP_RESPONSE_200, $data);
    }
}